<?php

/**
*
*/
class User
{

    private $protectedField = array('ID');
    private $errorArray = array();

    function __construct()
    {
    }


    private function removeProtected($d)
    {
        unset($d['protectedField']);
        unset($d['errorArray']);

        foreach ($d as $key => $value) {
            if (in_array($key, $this->protectedField)) {
                unset($d[$key]);
            }
        }

        return $d;
    }

    public function setError($err)
    {
        $this->errorArray[] = $err;
    }

    public function getError()
    {
        if (count($this->errorArray)) {
            return $this->errorArray;
        } else {
            return 0;
        }
    }

    public function login($d)
    {

        global $wpdb;
        $prefix = $wpdb->prefix;
        $table = $prefix.'users';

        // print_r($d);exit;


        if (!isset($d['login']) || !$d['login']) {
            $this->setError('Wprowadx login');
        }

        if (!isset($d['password']) || !$d['password']) {
            $this->setError('Wprowadx haslo');
        }

        $row = $wpdb->get_row("SELECT * FROM $table WHERE `user_email` = '".$d['login']."' and  `user_pass` = '".md5($d['password'])."' ");

        // print_r($row);exit;


        return $row->ID;
    }

    public function save()
    {

        global $wpdb;
        $prefix = $wpdb->prefix;

        $d = $this->removeProtected(get_object_vars($this));

        $metaRows = $d['meta'];
        unset($d['meta']);
        $userRows = $d;
        unset($d);

        $user_id = $this->createUser($userRows);


        if (!$user_id) {
            $this->setError('The user with the login or e-mail already exists');
            return;
        }
        foreach ($metaRows as $key => $value) {
            $this->createUserMeta($user_id, $key, $value);
        }

        $this->setUserRole($user_id);

        return $user_id;
    }

    public function createUserMeta($user_id, $key, $val)
    {

        global $wpdb;
        $prefix = $wpdb->prefix;

        $array = array(
                'user_id' => $user_id,
                'meta_key' => $key,
                'meta_value' => $val
            );

        $wpdb->insert(
            $prefix.'usermeta',
            $array
        );

        return $wpdb->insert_id;
    }

    public function updateUserMeta($user_id, $key, $val)
    {

        global $wpdb;
        $prefix = $wpdb->prefix;
        $um = get_user_meta($user_id,$key);



        if(!count($um)){
            $out = $this->createUserMeta($user_id,$key,$val);
        }else{
            $out = update_user_meta($user_id, $key, $val);
        }



        return $out;
    }

    private function createUser($rows)
    {

        global $wpdb;
        $prefix = $wpdb->prefix;
        $table = $prefix.'users';

        $mylink = $wpdb->get_row("SELECT * FROM $table WHERE  `user_email` = '".$rows['user_email']."' ");



        if ($mylink) {
            return;
        }

        $array = array();
        foreach ($rows as $key => $value) {
            $array[$key] = $value;
        }

        $wpdb->insert(
            $table,
            $array
        );

        return $wpdb->insert_id;
    }

    public function setUserRole($id)
    {

        $u = new WP_User($id);
        $u->set_role('tw-user');
    }

    public function current(){
      $user = wp_get_current_user();
      $user->meta = get_user_meta($user->ID);

      return $user;
    }

    public function get_user_by_meta_data( $meta_key, $meta_value ) {


        global $wpdb;


        // Query for users based on the meta data
        $user_query = new WP_User_Query(
            array(
                'meta_key'    =>    $meta_key,
                'meta_value'    =>  $meta_value
            )
        );
        // Get the results from the query, returning the first user
        $users = $user_query->get_results();
        if($users){
            return $users[0]->data;   
        }else{
            return false;
        }


    }
}
