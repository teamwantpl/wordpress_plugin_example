<?php

/**
 *
 */

 use Dompdf\Dompdf;



class Nonce_exampleController
{

	function __construct()
	{

	}

	public function index(){
		global $twPanel;



		$twPanel->twPanel->View(array(
			'Nonce_example/index',
			// 'q' => $q
		));

	}

	public function create_pdf(){
		global $twPanel;

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {


		  if(wp_verify_nonce($_REQUEST['action_field'], 'create_pdf')){

				// second validation
				if(!wp_verify_nonce($_REQUEST['action'], $twPanel->twPanel->getLink('Nonce_example/create_pdf/'))){
					die('Error 88');
				}

				$html = '';
				$html .= 'Filename: '.$_POST['filename'].'<br />';
				$html .= 'Content: '.$_POST['content'].'<br />';

				$dompdf = new DOMPDF();
				$dompdf->loadHtml($html);
				$dompdf->render();
				$dompdf->stream($_POST['filename'].".pdf", array("Attachment"=>0));

			}else{
				die('Error 83');
			}

		}else{
			die('The request method is not allowed here');
		}
	}

}
