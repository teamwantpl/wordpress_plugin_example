<?php
global $twPanel;
get_header();

?>

<form class="" action="<?php echo $twPanel->twPanel->getLink('Nonce_example/create_pdf/') ?>" method="post">

<?php wp_nonce_field('create_pdf', 'action_field'); ?>
<?php wp_nonce_field($twPanel->twPanel->getLink('Nonce_example/create_pdf/'), 'action'); ?>


<div class="">
	<label>Please add PDF file name</label>
	<input type="text" name="filename" value="">
</div>

<div class="">
	<label>Content</label>
	<textarea name="content" placeholder="add some pdf text" rows="8" cols="80"></textarea>
</div>

	<button type="submit" name="button">Do It</button>

</form>
