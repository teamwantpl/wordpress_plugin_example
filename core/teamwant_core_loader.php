<?php
/**
* My own loader :)
*/
class TeamwantCoreLoader
{

    public $groupName = 'tw-user';
    public $data;
    public $query;
    public $renderView = 0;
    public $renderController = 0;

    public $twdata = '';

    public function __construct()
    {
            $this->query = $_GET = $this->sanitize($_GET);
            $this->data = $_POST = $this->sanitize($_POST);

    }

    public function sanitize ($value) {
        // sanitize array or string values
        if (is_array($value)) {
            array_walk_recursive($value, 'self::sanitize_value');
        }
        else {
            sanitize_value($value);
        }

        return $value;
    }

    public function sanitize_value (&$value) {
        $value = trim(htmlspecialchars($value));
    }


    public function test()
    {
        echo "test success";
        exit;
    }

    public function installDatabase()
    {
        global $wpdb;

        if (!$this->checkDbTable('tw_user_order')) {
            $table = $wpdb->prefix.'tw_user_order';

            $sql[] = "CREATE TABLE IF NOT EXISTS `$table` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `price` DECIMAL(16,4) NOT NULL , `priceCode` VARCHAR(10) NOT NULL , PRIMARY KEY (`id`))ENGINE = InnoDB;";
            $sql[] = "ALTER TABLE `$table` ADD `user_id` INT(11) NOT NULL AFTER `id`, ADD INDEX (`user_id`);";

            foreach ($sql as $key => $value) {
                $wpdb->get_results($value);
            }

            return $this->installDatabase();
        }

        return;
    }

    public function addUserGroup()
    {
        add_role('tw-user', 'Teamwant User System');
        add_role('tw-employee', 'Teamwant Pracownik');
        add_role('tw-user-moderator', 'Teamwant Moderator');

        remove_role('subscriber');
        remove_role('contributor');
        remove_role('author');
        remove_role('editor');
    }


		//deprecated
    public function addPages()
    {

        // $login = $this->getPage('login');

    }


    public function View($data)
    {



        if (isset($data[0]) && $file = trim($data[0])) {
            $this->renderView = $file;
            $file = HOME_PATH.'/public/View/'.$file.'.php';

            if (!file_exists($file)) {
							echo $file;
                echo "no found view file, 409error";
                exit;
            }

            unset($data[0]);

            $this->twdata = $data;
            // ob_start();

            // // require_once($file);
            // include $file;
            // echo ob_get_clean();
            // ob_end_flush();


            // exit;

            add_filter( 'template_include', function() use ($file) {
                return $file;
            });

        } else {
            echo "no load view file";
            exit;
        }
    }

    public function getViewData()
    {
        return $this->twdata;
    }


    // array(
    //     'order/index',
    //     'val1'=>'val1',
    //     'val2'=>'val2'
    //     )
    public function extendView($data)
    {
        if (isset($data[0]) && $file = trim($data[0])) {
            $file = get_template_directory().'/tw-panel/'.$file.'.php';

            if (!file_exists($file)) {
                echo "no found extend view file, 410error";
                exit;
            }

            unset($data[0]);

            include $file;
        } else {
            echo "no load extend view file";
            exit;
        }
    }

    private function __loadPage($name)
    {
        echo "deprecated method";
        exit;

        $file = get_template_directory().'/tw-panel/'.$name.'.php';

        if (!file_exists($file)) {
            wp_redirect(home_url());
            exit;
        }

        $page = $this->getPage($name, 0)[0];
        if (!$page) {
            wp_redirect(home_url());
            exit;
        }

        ob_start();
        include $file; // PATH
        $data = ob_get_clean();

        echo $data;
        ob_end_flush();
        exit;
    }

    public function loadController($name, $fn = '')
    {


        $page = $this->getPage($name, 0)[0];
        if (!$page) {
            $this->addPages();
            wp_redirect(home_url());
            exit;
        }

        $controller_name = ucfirst(strtolower($name)).'Controller';
        $this->renderController = $controller_name;
        $controller_file = HOME_PATH.'/public/Controller/'.$controller_name.'.php';
        $model_file = HOME_PATH.'/public/Model/'.ucfirst(strtolower($name)).'.php';

        if (!file_exists($controller_file)) {
					echo $controller_file;
            echo 'No found Controller';
            exit;
        } else {
            // ładowanie modelu
            if (file_exists($model_file)) {
                require_once $model_file;
            }

            // ładowanie kontrollera
            require_once $controller_file;
            $controller = new $controller_name();

            if (method_exists($controller, 'initialize')) {
                $controller->initialize();
            }

            if ($fn != '') {
                if (method_exists($controller, $fn)) {
                    $controller->$fn();
                } else {
                    echo 'No found Function';
                    exit;
                }
            } else {
                if (method_exists($controller, 'index')) {
                    $controller->index();
                } else {
                    echo 'No found Function';
                    exit;
                }
            }
        }

        return;
    }

    public function loadModel($name)
    {

        $model_file = plugin_dir_path(__FILE__).'public/Model/'.ucfirst(strtolower($name)).'.php';

        if (!file_exists($model_file)) {
            echo 'No found Model';
            exit;
        } else {
            // ładowanie modelu
            require_once $model_file;
        }

        return;
    }


    private function checkDbTable($table)
    {
        global $wpdb;

        $table = $wpdb->prefix.$table;
        $myrows = $wpdb->get_results("DESCRIBE `$table`");


        if ($myrows) {
            return 1;
        } else {
            return 0;
        }
    }

    private function checkDbColumn($table, $field)
    {
        global $wpdb;

        $table = $wpdb->prefix.$table;
        $myrows = $wpdb->get_results("SHOW COLUMNS FROM `$table` LIKE '$field'");

        if ($myrows) {
            return $myrows;
        } else {
            return 0;
        }
    }


    public function getPage($name, $create = 1)
    {
        global $wpdb;

        $link = home_url('?tw-panel=' . $name);

        $table = $wpdb->prefix.'posts';
        $myrows = $wpdb->get_results("SELECT * FROM `$table` WHERE `post_type` = 'tw-panel' and post_title = '$name' ");

        if ($myrows || $create == 0) {
            return $myrows;
        } else {
            $sql = "INSERT INTO `$table` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES (NULL, '1', '2017-10-13 20:50:17', '0000-00-00 00:00:00', '', '$name', '', 'publish', 'closed', 'closed', '', '$name', '', '', '2017-10-13 20:50:17', '2017-10-13 20:50:17', '', '0', '$link', '0', 'tw-panel', '', '0')";

            $wpdb->query($sql);

            $post = $this->getPage($name)[0];
            apply_filters('_get_page_link', $link, $post->ID);


            return $post;
        }
    }

    public function getLink($name)
    {
        return home_url().'/tw-panel/'.$name;
    }

    public function getAdminLink($name, $vars = '')
    {
        $dir = 'tw-login-system/admin/'.$name.'.php';
        return get_admin_url().'admin.php?page='.urlencode($dir).(($vars)? '&'.$vars:'');
    }

    public function isPost()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            return 1;
        } else {
            return 0;
        }
    }

    public function isGet()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            return 1;
        } else {
            return 0;
        }
    }

    public function notFound()
    {
        echo "page not found";
        status_header(404);
        nocache_headers();
        exit;
        include(get_query_template('404'));
        die();
    }


    public function loginSettings(){
      return json_decode(get_option('twloginsystem_setting'));
    }


    public function initSession(){

        if(!session_id()) {
            session_start();
        }

        // if (session_status() == PHP_SESSION_NONE) {
        //     session_start();
        // }

        return 1;
    }

    public function getSession(){
        $this->initSession();
        return $_SESSION;

    }

    public function setSession($key,$val){
        $this->initSession();
        $_SESSION[$key] = $val;

        return $_SESSION;
    }

    public function getFormError(){
        $this->initSession();
        return ((isset($_SESSION['Form_Error']))? $_SESSION['Form_Error']: '');
    }

    public function setFormError($val,$key = ''){
        $this->initSession();

        if($key){
            $_SESSION['Form_Error'][$key] = $val;
        }else{
            $_SESSION['Form_Error'][] = $val;
        }

        return 1;
    }

    public function clearFormError(){
        $this->initSession();

        if(isset($_SESSION['Form_Error'])){
            unset($_SESSION['Form_Error']);
        }

        return 1;
    }
}
