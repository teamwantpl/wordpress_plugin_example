<?php

/**
* This is a plugin core class
*/

require_once __DIR__.'/teamwant_core_loader.php';
require_once __DIR__.'/teamwant_form.php';

class Work_Plugin_Initialize{

	public $twPanel;
	public $twForm;


	public function __construct(){

		$this->twPanel = new TeamwantCoreLoader();
		$this->twForm = new twForm();

		global $twPanel;
		$twPanel = $this;

		add_filter('query_vars', array($this, 'myplugin_register_query_vars'));
		add_action('init', array($this, 'myplugin_rewrite_tag_rule'), 10, 0);
		add_action('pre_get_posts', array($this, 'myplugin_pre_get_posts'), 1);

		$this->addPages();
	}

	public function debug(){
		var_dump($_REQUEST);
		var_dump($_GET);
		var_dump($_POST);
		// var_dump(get_query_var( 'tw-panel' ));
		// exit;
	}

	public function myplugin_register_query_vars($vars)
	{
	    $vars[] = 'tw-panel';
	    return $vars;
	}

	/**
	 * Add rewrite tags and rules
	 */
	public function myplugin_rewrite_tag_rule()
	{
	    add_rewrite_tag('%tw-panel%', '([^&]+)');
	    add_rewrite_tag('%fn%', '([^&]+)');

	    add_rewrite_rule('^tw-panel/([^/]*)/([^/]*)/?', 'index.php?tw-panel=$matches[1]&fn=$matches[2]', 'top');
	    add_rewrite_rule('^tw-panel/([^/]*)/?', 'index.php?tw-panel=$matches[1]', 'top');
	    add_rewrite_rule('^tw-panel/?', 'index.php?tw-panel=""', 'top');

	    flush_rewrite_rules();
	}


	public function myplugin_pre_get_posts($query)
	{
	    $tw  = get_query_var('tw-panel');
	    $tw2 = get_query_var('fn');

	    if ($tw == '\"\"') {
	        wp_redirect($this->twPanel->getLink('login/index'));
	        exit;
	    }

	    if (!empty($tw)) {
	        // $this->twPanel->loadPage($tw);
	        $this->twPanel->loadController($tw, $tw2);
	    }

	    return 1;
	}

	public function page_generator(){

	}

	/**
	 * this function adds new pages to website
	 */
	public function addPages(){
		$this->twPanel->getPage('Nonce_example');
		return 1;
	}

}
